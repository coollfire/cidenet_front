import { userDb } from './user.interface';
export interface Round {
    round_id?: number;
    entry: string;
    leave: string;
    create_at?: Date;
    status?: boolean;
    area: string;
    user?: userDb;
}