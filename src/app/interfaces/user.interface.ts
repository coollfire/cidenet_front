import { Round } from "./round.interface";

export interface userDb {
    user_id?:        number;
    passw?:          string;
    name_one?:       string;
    name_others?:    string;
    last_name_one?:  string;
    last_name_two?:  string;
    country?:        string;
    identification?: string;
    numId?:          string;
    email?:          string;
    status?:         boolean;
    create_at?:      Date;
    rounds?:         Round[];
}


