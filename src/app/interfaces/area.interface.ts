export interface area {
    area_id:   number;
    nameArea:  string;
    create_at: Date;
    status:    boolean;
}
