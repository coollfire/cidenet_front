import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './pages/register/register.component';
import { UserComponent } from './pages/user/user.component';
import { AreaComponent } from './pages/area/area.component';

const routes: Routes = [
  {
    path: 'user',
    component: UserComponent

  },
  {
    path: 'register/:id',
    component: RegisterComponent
  },
  {
    path: 'area',
    component: AreaComponent
  },
  {
    path: '**',
    redirectTo: 'user'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
