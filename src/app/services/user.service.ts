import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userDb } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private url = "http://localhost:8080/prueba/api";

  constructor(private http: HttpClient) { }



  allUsers() {
    return this.http.get<userDb[]>(this.url + "/user/all")
  }

  save(user: userDb) {

    return this.http.post(this.url + "/user/save", user);

  }

  findbyEmail(email: string) {
    this.http.post<userDb>(this.url + "/user/email", { "data": email }).subscribe(resp => {
      console.log(resp);

    })
  }

  delete(id: number) {
    if (window.confirm('Deseas eliminar el usuario')) {
      return this.http.delete(this.url + "/user/delete", { body: { 'id': id } })
    }
    return;
  }

  findById(id: number) {
    return this.http.post<userDb>(this.url + "/user/find", { "id": id })
  }
}
