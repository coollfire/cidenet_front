import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Round } from '../interfaces/round.interface';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private url = "http://localhost:8080/prueba/api";

  constructor(private http: HttpClient) { }

  save(registro: Round) {

    return this.http.post(this.url + "/round/save", registro);

  }

  delete(id: number) {
    if (window.confirm('Deseas eliminar el Registro')) {
      return this.http.delete(this.url + "/round/delete", { body: { 'id': id } })
    }
    return;

  }
}
