import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { userDb } from '../../interfaces/user.interface';
import { Round } from '../../interfaces/round.interface';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  id: number;

  user!: userDb;
  registros!: Round[];
  registro!: Round;

  constructor(private rutaActiva: ActivatedRoute,
    private userService: UserService,
    private resgiterService: RegisterService,
    private fb: FormBuilder) {
    this.id = this.rutaActiva.snapshot.params['id'];

  }

  ngOnInit(): void {

    this.getUser();
  }


  formulario: FormGroup = this.fb.group({
    entry: ['', [Validators.required, this.validarFechas]],
    area: ['', [Validators.required]],
    leave: ['', [Validators.required]],
  });

  //traer registros de un suario desde base de datos
  getUser() {

    this.userService.findById(this.id).subscribe(resp => {
      this.user = resp;
      this.registros = resp.rounds!;
      console.log(resp);

    }, err => {
      console.log(err);
    })

  }

//guarda nuevo registro
  enviar() {
    let entry: string = this.formatDate(new Date(this.formulario.value['entry']));
    let leave: string = this.formatDate(new Date(this.formulario.value['leave']));
    let resgiter!: Round;
    let user: userDb = {
      user_id: this.id
    };

    resgiter = {
      entry: entry,
      leave: leave,
      area: this.formulario.value['area'],
      user: user
    }


    this.resgiterService.save(resgiter).subscribe(resp => {
      console.log(resp);

    }, err => {
      console.log(err);

    })


    this.formulario.reset();
    window.location.reload();




  }

  //Metodo eliminar un regitrp
  deleteRegister(id: number) {
    this.resgiterService.delete(id)?.subscribe(resp => {
      console.log('resp');
      console.log(resp);
      window.location.reload();
    }, err => {
      console.log('err');
      console.log(err);
      window.location.reload();
    })
  }


  //metodo para Validar que la fechas en encuntren en un rango especificado
  validarFechas(control: FormControl) {

    const dateEntry: Date = new Date(control.value);
    const date: Date = new Date();
    const addMlSeconds = 1000 * 60 * 60 * 24 * 30;
    const resta = date.getTime() - addMlSeconds;
    const dateMenos: Date = new Date(resta);

    if (dateEntry < date) {

      if (dateEntry > dateMenos) {
        return null;
      }

      return { Fecha: 'fecha no permitida' };

    }
    return { Fecha: 'fecha no permitida' };




  }

  //Metodos para cabiar formato de fecha yyyy-mm-dd hh:mm:ss
  padTo2Digits(num: any) {
    return num.toString().padStart(2, '0');
  }

  formatDate(date: any) {
    return (
      [
        date.getFullYear(),
        this.padTo2Digits(date.getMonth() + 1),
        this.padTo2Digits(date.getDate()),
      ].join('-') +
      ' ' +
      [
        this.padTo2Digits(date.getHours()),
        this.padTo2Digits(date.getMinutes()),
        this.padTo2Digits(date.getSeconds()),
      ].join(':')
    );
  }
 //--------------------------



 //Metodo para validar campos del formularios
  validarCampo(campo: string) {
    return this.formulario.controls[campo].errors && this.formulario.controls[campo].touched;

  }
}
