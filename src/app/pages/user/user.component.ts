import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { userDb } from 'src/app/interfaces/user.interface';
import { Country } from '../../interfaces/country.interface';
import { typeId } from '../../interfaces/typeId.interface';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users: userDb[] = [];
  loading: boolean = true;
  userSelec!: userDb;
  edit: boolean = false;
  userEdit!: userDb;
  searchText: string = "";
  page: number = 1;
  usuarioDialog: boolean = false;


  constructor(private fb: FormBuilder, private router: Router, private userService: UserService) {

  }

  ngOnInit(): void {
    this.getAllUsers();
  }


  //Metodo para traer los usuario de la BD
  getAllUsers() {

    this.userService.allUsers().subscribe(resp => {
      console.log(resp);
      this.users = <userDb[]>resp
    }, err => {
      alert('Error al Cargar la data');
    });
  }


  formulario: FormGroup = this.fb.group({
    passw: ['', [Validators.maxLength(20)]],
    name_one: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Z ]+$')]],
    name_others: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[A-Z ]+$')]],
    last_name_one: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Z ]+$')]],
    last_name_two: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Z ]+$')]],
    country: ['', Validators.required],
    identification: ['', Validators.required],
    numId: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9]+$')]],
  });


  //metodo creo y edita nuevos usuario
  enviar() {

    console.log(this.formulario.value);

    if (this.formulario.invalid) {
      this.formulario.markAllAsTouched();
      return;
    }

    if (this.edit) {

      this.userEdit.passw = this.formulario.value['passw'];
      this.userEdit.name_one = this.formulario.value['name_one'];
      this.userEdit.name_others = this.formulario.value['name_others'];
      this.userEdit.last_name_one = this.formulario.value['last_name_one'];
      this.userEdit.last_name_two = this.formulario.value['last_name_two'];
      this.userEdit.country = this.formulario.value['country'];
      this.userEdit.identification = this.formulario.value['identification'];
      this.userEdit.numId = this.formulario.value['numId'];
      this.userService.save(this.userEdit).subscribe(resp => {



      }, err => {
        console.log(err);
      });
      this.edit = false;
    } else {

      this.userService.save(this.formulario.value).subscribe(resp => {
        console.log('resp');
        console.log(resp);


      }, err => {
        console.log('err');
        console.log(err);
      });
    }
    this.formulario.reset();
    window.location.reload();
  }

  //Valida camppos del fromulario
  validarCampo(campo: string) {
    return this.formulario.controls[campo].errors && this.formulario.controls[campo].touched;

  }


  //elimina un usuario
  deleteUser(id: number) {
    this.userService.delete(id)?.subscribe(resp => {
      console.log('resp');
      console.log(resp);
      window.location.reload();
    }, err => {
      console.log('err');
      console.log(err);
      window.location.reload();
    })
  }


  //seta la data de un usuario en el formulario
  editar(user: userDb) {

    this.userEdit = user;
    this.formulario.controls['passw'].setValue(user.passw);
    this.formulario.controls['name_one'].setValue(user.name_one);
    this.formulario.controls['name_others'].setValue(user.name_others);
    this.formulario.controls['last_name_one'].setValue(user.last_name_one);
    this.formulario.controls['last_name_two'].setValue(user.last_name_two);
    this.formulario.controls['country'].setValue(user.country);
    this.formulario.controls['identification'].setValue(user.identification);
    this.formulario.controls['numId'].setValue(user.numId);
    this.edit = true;

  }


}
